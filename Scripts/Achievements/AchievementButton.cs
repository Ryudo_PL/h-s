﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AchievementButton : MonoBehaviour {

	public GameObject achievementList; // odniesienie do danej kategorii achievementow
	public Sprite neutral, highlight; // odniesienie do spritow wskazujacych na czy dany przycisk jest wybrany czy nie

	private Image sprite; //odniesienie do czesci prefab przycisku bedacej Image

	void Awake()
	{
		sprite = GetComponent<Image> (); // Zczytywanie komponentu Image przycisku
	}

	public void Click() // funkcja wzywana podczas klikniecia w button
	{
		if (sprite.sprite == neutral)  // jezeli sprite buttona jest ustawiony na niewybrany
		{
			sprite.sprite = highlight; // to po kliknieciu ustaw na wybrany
			achievementList.SetActive (true); // i pokaz liste achievementow z tej listy
		} else  
		{
			sprite.sprite = neutral;
			achievementList.SetActive(false);
		}
	}
}

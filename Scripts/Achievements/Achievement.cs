﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Achievement
{
	private string name; //nazwa achievementu

	public string Name
	{
		get{return name;}
		set{name = value;}
	}

	private string description; //opis achievementu

	public string Description
	{
		get{ return description;}
		set{ description = value;}
	}

	private bool unlocked; // bool czy achievement jest odblokowany

	public bool Unlocked
	{
		get{ return unlocked;}
		set{ unlocked = value;}
	}

	private int points; // punkty jakie zdobywamy za achievement

	public int Points
	{
		get{ return points;}
		set{points = value;}
	}

	private int spriteIndex; // odniesienie do numeru sprite w zbiorze - obrazek achievementa

	public int SpriteIndex
	{
		get{return spriteIndex;}
		set{spriteIndex = value;}
	}
	private GameObject achievementRef; // aby pokazac w grze jaki achievement sie zdobylo

	private List<Achievement> dependencies = new List<Achievement> (); //lista achievementow ktore sa zalezne jedne od drugich

	private string child; // nazwa achievementu, ktory musimy najpierw zdobyc

	public string Child
	{
		get{return child;}
		set{child = value;}
	}


	public Achievement(string name,string description,int points, int spriteIndex, GameObject achievementRef) //konstruktor
	{
		this.name = name;
		this.description = description;
		this.unlocked = false;
		this.points = points;
		this.spriteIndex = spriteIndex;
		this.achievementRef = achievementRef;
		LoadAchievement ();
	}

	public void AddDependency(Achievement dependency)
	{
		dependencies.Add (dependency); //dodanie achievementa do listy zaleznych achievementow
	}

	public bool EarnAchievement()
	{
		if (!unlocked && !dependencies.Exists(x => x.unlocked == false)) 
			/*jezeli achievement nie jest zdobyty oraz  */
		{
			achievementRef.GetComponent<Image>().sprite = AchievementManager.Instance.unlockedSprite; //przydziel sprite unlocked
			SaveAchievement(true); //zapisz achievement
			if(child != null) //jezeli istnieja zalezne achievementy
			{
				AchievementManager.Instance.EarnAchievement(child); 
			} //odniesienie do funckji w pliku Achievement Manager ->  by pokazac zdobyty achievement (child)
			return true;
		}
		return false;
	}

	public void SaveAchievement(bool value)
	{
		unlocked = value; //jezeli zapisac to unlocked przyjmuje wartosc 1
		int tmpPoints = PlayerPrefs.GetInt ("Points"); //odczytanie ile jest punktow juz zdobytych lacznie
		PlayerPrefs.SetInt ("Points", tmpPoints += points); //zasavowanie zdobytej teraz liczby punktow do lacznej
		PlayerPrefs.SetInt (name, value ? 1 : 0); //zapisanie achievementa jezeli ma wartosc 1
		PlayerPrefs.Save (); //zapisanie PlayerPrefs
	}

	public void LoadAchievement()
	{
		unlocked = PlayerPrefs.GetInt (name) == 1 ? true : false; //wczytywanie wszelkich achievementow, ktore maja wartosc 1
		if (unlocked)  //jezeli juz odblokowane
		{
			AchievementManager.Instance.textPoints.text = "Points: " + PlayerPrefs.GetInt("Points"); //wczytanie zdobytej lacznej liczby achievementow
			achievementRef.GetComponent<Image>().sprite = AchievementManager.Instance.unlockedSprite; //przydzielenie odblokowanym achievementow sprite unlocked

		}
	}
}

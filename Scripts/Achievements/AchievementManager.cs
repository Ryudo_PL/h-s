﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class AchievementManager : MonoBehaviour {

	public GameObject achievementPrefab; // odniesienie do achievement prefab
	public Sprite[] sprites; // zbior spritow bedacych na achievementach
	private AchievementButton activeButton; // odniesienie do przycisku - wyboru kategorii achievementow
	public ScrollRect scrollRect; //odniesienie do obszaru, ktory mozna przesuwac

	public GameObject achievementMenu; // odniesienie do calego UI odosnie achievementow
	public GameObject visualAchievement; //achievement pokazywany na ekranie

	public Dictionary<string,Achievement> achievements = new Dictionary<string, Achievement>();

	public Sprite unlockedSprite; //odblokowany achievement -> sprite

	public Text textPoints; // Laczna liczba punktow

	private int fadeTime = 2; //czas potrzebny do zaniku wyskakujacego achievementa

	private static AchievementManager instance; //tzw. singleton

	public static AchievementManager Instance
	{
		get
		{
			if(instance == null)
			{
				instance = GameObject.FindObjectOfType<AchievementManager>();
			}
			return AchievementManager.instance;
		}
	}

	// Use this for initialization
	void Start () {
		//REMEMBER TO REMOVE
		PlayerPrefs.DeleteAll (); // skasuj wszelki zapisane info odnosnie zapisanych punktow, achievementow

		activeButton = GameObject.Find ("GeneralBtn").GetComponent<AchievementButton> ();
		CreateAchievement ("General","Press W","Press W to unlock this achievement",10,0);

		foreach(GameObject achievementList in GameObject.FindGameObjectsWithTag("AchievementList"))
		{
			achievementList.SetActive(false);
		}
		activeButton.Click ();
		achievementMenu.SetActive (false);
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.I))  //jak nacisniesz I
		{
			achievementMenu.SetActive(!achievementMenu.activeSelf); // to zrobi sie przeciwienstwo obecnego stanu (Hide <-> Show)
		}
		if (Input.GetKeyDown (KeyCode.W)) //jak nacisniesz W
		{
			EarnAchievement("Press W"); // zdobywasz jeden z achievementow
		}
		  
	}

	public void EarnAchievement(string title)
	{
		if(achievements[title].EarnAchievement()) //po tytule dictionary przyporzadkowuje achievement
		{
			GameObject achievement = (GameObject)Instantiate(visualAchievement);
			SetAchievementInfo("EarnCanvas",achievement,title);
			textPoints.text = "Points: " + PlayerPrefs.GetInt("Points");
			StartCoroutine(FadeAchievement(achievement));

		}
		  

	}

	public IEnumerator HideAchievement(GameObject achievement) //Funckja powodujaca ze wyswietlany achievement znika
	{
		yield return new WaitForSeconds (3); //czekaj 3 sekundy
		Destroy (achievement); //"zniszcz" achievement
	}

	public void CreateAchievement(string parent, string title, string description, int points, int spriteIndex, string[] dependencies = null)
	{
		GameObject achievement = (GameObject)Instantiate (achievementPrefab);
		Achievement newAchievement = new Achievement (name, description, points, spriteIndex, achievement);

		achievements.Add (title, newAchievement); //dodaj podany achievement do zbioru
		SetAchievementInfo (parent, achievement, title); 

		if (dependencies != null) 
		{
			foreach(string achievementTitle in dependencies)
			{
				Achievement dependency = achievements[achievementTitle];
				dependency.Child = title;
				newAchievement.AddDependency(dependency);
			}
		}
	}

	public void SetAchievementInfo(string parent, GameObject achievement, string title)
	{
		achievement.transform.SetParent(GameObject.Find(parent).transform);
		achievement.transform.localScale = new Vector3 (1, 1, 1);
		achievement.transform.GetChild (0).GetComponent<Text> ().text = title;
		achievement.transform.GetChild (1).GetComponent<Text> ().text = achievements[title].Description;
		achievement.transform.GetChild (2).GetComponent<Text> ().text = achievements[title].Points.ToString ();
		achievement.transform.GetChild (3).GetComponent<Image> ().sprite = sprites [achievements[title].SpriteIndex];
		                                                                                        

	}

	public void ChangeCategory(GameObject button) 
	{
		AchievementButton achievementButton = button.GetComponent<AchievementButton> ();

		scrollRect.content = achievementButton.achievementList.GetComponent<RectTransform> (); //scrollRect potrzebuje RectTransform
		achievementButton.Click ();
		activeButton.Click ();
		activeButton = achievementButton;
	}

	private IEnumerator FadeAchievement(GameObject achievement) //funkcja zanikania achievementu
	{
		CanvasGroup canvasGroup = achievement.GetComponent<CanvasGroup> (); //odnosnik do canvas group-> by wszystkim child razem zmieniac wartosc aplha

		float rate = 1.0f / fadeTime;
		int startAlpha = 0;
		int endAlpha = 1;

		for (int i = 0; i < 2; i++) 
		{
			float progress = 0.0f;
			while(progress < 1.0)
			{
				canvasGroup.alpha = Mathf.Lerp(startAlpha,endAlpha,progress);
				progress += rate * Time.deltaTime;
				
				yield return null;
			}
			
			yield return new WaitForSeconds (2);
			
			startAlpha = 1;
			endAlpha = 0;
		}

		Destroy (achievement);
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnterExitDungeon : MonoBehaviour {

	//reference to Dungeon Generator script
	private DungeonGenerator dungeonGenerator;

	//reference to PLayerController script
	private PlayerController player;

	//reference to Text component used to display dungeon's name
	private Text dungeonText;

	//string that will hold dungeon's name
	private string dungeonName = "Crypt";

	//name of our world map
	private string worldName = "Wildlands";

	//array of dungeons' names
	private string[] dungeonNames;

	//bool to check whether player is entering a dungeon
	public bool transitionToDungeon;

	//reference to ScreenFader script
	private ScreenFader fadeImage; 

	private Text tooltipText;

	//how much time it takes to display and hide dungeon name text
	public float fadeDuration = 2f;

	void Start () 
	{
		//Assigning variables/scripts
		dungeonGenerator = GameObject.FindObjectOfType<DungeonGenerator>();
		player = GameObject.FindObjectOfType<PlayerController>();
		dungeonText = GameObject.Find("DungeonName").GetComponent<Text>();
		dungeonText.enabled = false;
		fadeImage = GameObject.FindGameObjectWithTag("Fade").GetComponent<ScreenFader>();
		tooltipText = GameObject.FindGameObjectWithTag("Dungeon").GetComponent<Text>();
	}

	//It is called out on the mouse click
	void OnMouseUp()
	{
		if(gameObject.tag == "Exit" && player.canEnterDungeon == true)
		{
			fadeImage.fadeIn = false;
			StartCoroutine("FadeInOut");

		}
		if(gameObject.tag == "Enter" && player.canEnterDungeon == true)
		{
			fadeImage.fadeIn = false;
			StartCoroutine("FadeInOut");
		}
	}

	void OnMouseOver()
	{
		tooltipText.enabled = true;
		if(transitionToDungeon == true)
		{
			tooltipText.text = dungeonName;
		}
		else if(transitionToDungeon == false && gameObject.tag != "Chest")
		{
			tooltipText.text = worldName;
		}

	}

	void OnMouseExit()
	{
		tooltipText.enabled = false;
	}

	//Checks whether player is close enought to click on the entrance 
	void OnTriggerEnter(Collider other)
	{
		if((gameObject.tag == "Enter" || gameObject.tag == "Exit") && other.tag == "Player")
		{
			player.canEnterDungeon = true;
		}
	}

	//Checks whether player is far enough from the entrance, so it cannot enter the dungeon
	void OnTriggerExit(Collider other)
	{
		if((gameObject.tag == "Exit" || gameObject.tag == "Enter") && other.tag == "Player")
		{
			player.canEnterDungeon = false;
		}
	}

	private IEnumerator FadeInOut()
	{
		yield return new WaitForSeconds(1f);
		if(transitionToDungeon == true)
		{
			dungeonGenerator.EnterDungeon();
		}
		else
		{
			dungeonGenerator.ExitDungeon();
		}
		yield return new WaitForSeconds(fadeDuration);

		if(dungeonText.enabled == false)
		{
			if(transitionToDungeon == false)
			{
				dungeonText.enabled = true;
				dungeonText.text = worldName;
			}
			else
			{
				dungeonText.enabled = true;
				dungeonText.text = dungeonName;
			}
		}
		else
		{
			dungeonText.enabled = false;
		}
		yield return new WaitForSeconds(fadeDuration);
		dungeonText.enabled = false;
		fadeImage.fadeIn = true;
		yield return null;
	}
}

﻿using UnityEngine;
using System.Collections;

public class TopDownCamera : MonoBehaviour {

	public Transform target;


	[System.Serializable]
	public class PositionSettings
	{
		//distance from our target
		//bools for zooming and smooth following
		//min and max zoom settings

		public float distanceFromTarget = -10;
		public bool allowZoom = true;
		public float zoomSmooth = 100;
		public float zoomStep = 2;
		public float maxZoom = -5;
		public float minZoom = -15;
		public bool smoothFollow = true;
		public float smooth = 0.05f;

		[HideInInspector]
		public float newDistance = -10; //used for smooth zooming

	}

	[System.Serializable]
	public class OrbitSettings
	{
		//holding our current x,y rotation for the camera
		//bool for allowing orbit

		public float xRotation = -65;
		public float yRotation = -180;
		public bool allowOrbit = true;
		public float yOrbitSmooth = 0.5f;
	}

	[System.Serializable]
	public class InputSettings
	{
		public string MOUSE_ORBIT = "MouseOrbit";
		public string ZOOM = "Mouse ScrollWheel";
	}

	public PositionSettings position = new PositionSettings();
	public OrbitSettings orbit = new OrbitSettings();
	public InputSettings input = new InputSettings();

	Vector3 destination = Vector3.zero;
	Vector3 camVelocity = Vector3.zero;
	Vector3 currentMousePosition = Vector3.zero;
	Vector3 previousMousePosition = Vector3.zero;

	float mouseOrbitInput;
	float zoomInput;

	void Start()
	{
		//setting camera target
		//SetCameraTarget(target);

		if(target)
		{
			MoveToTarget();
		}
	}

	public void SetCameraTarget(Transform t)
	{
		//if we want to set target at runtime

		target = t;

		if(target == null)
		{
			Debug.LogError("Your camera needs a target");
		}
	}

	void GetInput()
	{
		//filling the values for our input

		mouseOrbitInput = Input.GetAxisRaw(input.MOUSE_ORBIT);
		zoomInput = Input.GetAxisRaw(input.ZOOM);
	}

	void Update()
	{
		//getting input and zooming

		GetInput();
		if(position.allowZoom)
		{
			ZoomInOnTarget();
		}
	}

	void FixedUpdate()
	{
		//move to target
		//look at target
		//orbit

		if(target)
		{
			MoveToTarget();
			LookAtTarget();
			MouseOrbitTarget();
		}
	}

	void MoveToTarget()
	{
		//handling getting our camera to target's destination

		destination = target.position;
		destination += Quaternion.Euler(orbit.xRotation,orbit.yRotation,0) * -Vector3.forward * position.distanceFromTarget;

		if(position.smoothFollow)
		{
			transform.position = Vector3.SmoothDamp(transform.position,destination,ref camVelocity,position.smooth);
		}
		else
		{
			transform.position = destination;
		}
	}

	void LookAtTarget()
	{
		//handling getting our camera to look at our target

		Quaternion targetRotation = Quaternion.LookRotation(target.position - transform.position);
		transform.rotation = targetRotation;
	}

	void MouseOrbitTarget()
	{
		//getting the camera to orbit around the target
		previousMousePosition = currentMousePosition;
		currentMousePosition = Input.mousePosition;

		if(mouseOrbitInput > 0)
		{
			orbit.yRotation += (currentMousePosition.x - previousMousePosition.x) * orbit.yOrbitSmooth;
		}
	}

	void ZoomInOnTarget()
	{
		//modifying the distance to target to be closer or further away

		position.newDistance += position.zoomStep * zoomInput;

		position.distanceFromTarget = Mathf.Lerp(position.distanceFromTarget,position.newDistance, position.zoomSmooth * Time.deltaTime);

		if(position.distanceFromTarget > position.maxZoom)
		{
			position.distanceFromTarget = position.maxZoom;
			position.newDistance = position.maxZoom;
		}
		if(position.distanceFromTarget < position.minZoom)
		{
			position.distanceFromTarget = position.minZoom;
			position.newDistance = position.minZoom;
		}
	}
}

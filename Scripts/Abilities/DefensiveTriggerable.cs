﻿using UnityEngine;
using System.Collections;

public class DefensiveTriggerable : MonoBehaviour {

	private SkinnedMeshRenderer[] skinnedMesh;
	public Shader hideShader;
	public float abilityTime = 10f;
	private Shader previousShader;

	// Use this for initialization


	public void Hide()
	{
		skinnedMesh = transform.GetComponentsInChildren<SkinnedMeshRenderer>();
		foreach(SkinnedMeshRenderer mesh in skinnedMesh)
		{
			previousShader = mesh.material.shader;
			mesh.material.shader = hideShader;
		}
		foreach(GameObject enemy in GameManager.instance.enemiesList)
		{
			enemy.GetComponent<AIPath>().isPlayerHiding = true;
			enemy.GetComponent<AIPath>().isWandering = true;
		}
		StartCoroutine("HidePlayer");
	}


	private IEnumerator HidePlayer()
	{
		yield return new WaitForSeconds(abilityTime);
		foreach(SkinnedMeshRenderer mesh in skinnedMesh)
		{
			mesh.material.shader = previousShader;
		}
		foreach(GameObject enemy in GameManager.instance.enemiesList)
		{
			enemy.GetComponent<AIPath>().isWandering = false;
			enemy.GetComponent<AIPath>().isPlayerHiding = false;
		}
	}
}

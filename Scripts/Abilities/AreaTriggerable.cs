﻿using UnityEngine;
using System.Collections;

public class AreaTriggerable : MonoBehaviour {

	public int damage;
	public float abilityRange;
	public float hitForce;
	public LayerMask enemyMask;
	private RaycastHit hit;
	private Ray ray;
	private PlayerController player;
	private int strength, stamina, agility,intellect;

	public void Launch(int skillNumber, GameObject skillHolder, GameObject skillPrefab)
	{
		switch(skillNumber)
		{
		case 1:
			if(Physics.Raycast(skillHolder.transform.forward,transform.forward,out hit,enemyMask))
			{
				if(hit.transform.GetComponent<EnemyController>())
				{
					hit.transform.GetComponent<EnemyController>().TakeDamage(damage);
				}
			}
			break;
		case 2:
			player = skillHolder.transform.GetComponentInParent<PlayerController>();
			strength = player.baseStrength;
			stamina = player.baseStamina;
			agility = player.baseAgility;
			intellect = player.baseIntellect;
			StartCoroutine("StatsToNormal");
			break;
		case 3:
			Collider[] enemiesInRange = Physics.OverlapSphere(skillHolder.transform.position,abilityRange,enemyMask);
			if(enemiesInRange.Length != 0)
			{
				foreach(Collider enemy in enemiesInRange)
				{
					StartCoroutine(DealConstantDamage(enemy));
				}
			}
			break;
		}
	}

	IEnumerator DealConstantDamage(Collider enemy)
	{
		for(float t = 0.0f; t < 4f; t += Time.deltaTime)
		{
			if(enemy.GetComponent<EnemyController>().CurrentHealth > 0)
			{
				enemy.GetComponent<EnemyController>().TakeDamage(damage);
				yield return new WaitForSeconds(1.5f);
			}
		}
		yield return null;
	}

	IEnumerator StatsToNormal()
	{
		player.baseStrength = 20;
		player.baseAgility = 20;
		player.baseStamina = 20;
		player.baseIntellect = 20;
		yield return new WaitForSeconds(10);
		player.baseStrength = strength;
		player.baseAgility = agility;
		player.baseStamina = stamina;
		player.baseIntellect = intellect;
	}
}

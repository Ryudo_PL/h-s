﻿using UnityEngine;
using System.Collections;

public abstract class Ability : ScriptableObject {

	public string abilityName = "New ability";
	public Sprite abilitySprite;
	public AudioClip abilitySound;
	public float abilityBaseCooldown = 1f;
	public int skillNumber;

	public abstract void Initialize(GameObject obj);
	public abstract void TriggerAbility(Transform skillHolder, int skillNumber);
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AbilityCooldown : MonoBehaviour {

	public KeyCode abilityKeyName = KeyCode.Q;

	//reference to image mask used for ability cooldown
	public Image darkMask;
	//public Text cooldownTextDisplay;

	[SerializeField] private Ability ability;

	private Image myButtonImage;
	private AudioSource abilitySource;
	private float cooldownDuration;
	private float nextReadyTime;
	private float cooldownTimeLeft;
	private GameObject player;
	private Transform skillHolder;


	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindObjectOfType<PlayerController>().gameObject;
		skillHolder = player.transform.FindChild("skillHolder");

		Initialize(ability,skillHolder.gameObject);
	}

	public void Initialize(Ability selectedAbility, GameObject skillHolder)
	{
		ability = selectedAbility;
		myButtonImage = GetComponent<Image>();
		abilitySource = GetComponent<AudioSource>();
		myButtonImage.sprite = ability.abilitySprite;
		darkMask.sprite = ability.abilitySprite;
		cooldownDuration = ability.abilityBaseCooldown;
		ability.Initialize(player);
		AbilityReady();
	}
	
	// Update is called once per frame
	void Update () 
	{
		bool cooldownComplete = (Time.time > nextReadyTime);
		if(cooldownComplete)
		{
			if(Input.GetKeyDown(abilityKeyName))
			{
				skillHolder = GetSkillHolderLocation();
				ButtonTriggered(skillHolder);
			}
		}
		else
		{
			Cooldown();
		}
	}

	//sets UI to show that ability is ready to use
	private void AbilityReady()
	{
		//cooldownTextDisplay.enabled = false;
		darkMask.enabled = false;
	}

	//handles ability's cooldown
	private void Cooldown()
	{
		cooldownTimeLeft -= Time.deltaTime;
		//float roundedCooldown = Mathf.Round(cooldownTimeLeft);
		//cooldownTextDisplay.text = roundedCooldown.ToString();
		darkMask.fillAmount = (cooldownTimeLeft / cooldownDuration);
	}

	private void ButtonTriggered(Transform skillHolder)
	{
		nextReadyTime = cooldownDuration + Time.time;
		cooldownTimeLeft = cooldownDuration;
		darkMask.enabled = true;
		//cooldownTextDisplay.enabled = false;

		//abilitySource.clip = ability.abilitySound;
		//abilitySource.Play();
		int skillNumber = ability.skillNumber;
		ability.TriggerAbility(skillHolder, skillNumber);
	}

	private Transform GetSkillHolderLocation()
	{
		return skillHolder.transform;
	}
}

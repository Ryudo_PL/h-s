﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu (menuName = "Abilities/AreaAbility")]
public class AreaAbility : Ability 
{
	public int damage = 50;
	public float abilityRange = 50f;
	public float hitForce = 2;
	public GameObject skillFx;
	private GameObject player;
	private GameObject skillHolder;
	public LayerMask enemyMask;

	private AreaTriggerable areaAbility;

	public override void Initialize(GameObject obj)
	{
		player = obj;

		areaAbility = obj.GetComponent<AreaTriggerable>();
		areaAbility.damage = damage;
		areaAbility.abilityRange = abilityRange;
		areaAbility.hitForce = hitForce;
		areaAbility.enemyMask = enemyMask;
	}

	public override void TriggerAbility(Transform skillHolder, int skillNumber)
	{
		GameObject skill = Instantiate(skillFx,skillHolder.position,Quaternion.identity,player.transform) as GameObject;


		//skill.AddComponent<SphereCollider>();
		areaAbility.Launch(skillNumber, skillHolder.gameObject, skill);
	}
}

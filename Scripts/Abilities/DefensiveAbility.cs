﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu (menuName = "Abilities/DefensiveAbility")]
public class DefensiveAbility : Ability 
{
	private GameObject player;
	public Shader hideShader;

	private DefensiveTriggerable defensiveTriggerable;

	public override void Initialize(GameObject obj)
	{
		player = obj;

		defensiveTriggerable = player.GetComponent<DefensiveTriggerable>();
		defensiveTriggerable.hideShader = hideShader;
	}

	public override void TriggerAbility(Transform skillHolder, int skillNumber)
	{
		defensiveTriggerable.Hide();
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FieldOfView : MonoBehaviour {

	public float viewRadius;
	[Range(0,360)]
	public float viewAngle;

	public LayerMask targetMask;
	public LayerMask obstacleMask;
	PlayerController characterController;
	Transform player;
	EnemyController enemyController;
	Animator enemyAnimator;
	public float range = 30;
	[HideInInspector]
	public List<Transform> visibleTargets = new List<Transform>();

	void Awake()
	{
		player = GameObject.FindGameObjectWithTag("Player").transform;
		characterController = player.GetComponent<PlayerController>();
		enemyController = gameObject.GetComponent<EnemyController>();
		enemyAnimator = gameObject.GetComponent<Animator>();
	}

	void Start() 
	{
		StartCoroutine ("FindTargetsWithDelay", .2f);
	}

	//Enumator to find targets which are visible (with a delay)
	IEnumerator FindTargetsWithDelay(float delay) {
		while (true) {
			yield return new WaitForSeconds (delay);
			FindVisibleTargets ();
		}
	}

	void FindVisibleTargets() {
		visibleTargets.Clear ();
		Collider[] targetsInViewRadius = Physics.OverlapSphere (transform.position, viewRadius, targetMask);
		if(targetsInViewRadius.Length == 0)
		{
			enemyAnimator.SetBool("Idle",true);
			gameObject.GetComponent<AIPath>().isWandering = true;
			//gameObject.GetComponent<AIPath>().canMove = false;
		}
		else
		{
			gameObject.GetComponent<AIPath>().isWandering = false;
			for (int i = 0; i < targetsInViewRadius.Length; i++) {
			Transform target = targetsInViewRadius [i].transform;
			Vector3 dirToTarget = (target.position - transform.position).normalized;
			if (Vector3.Angle (transform.forward, dirToTarget) < viewAngle / 2) {
				float dstToTarget = Vector3.Distance (transform.position, target.position);

				if (!Physics.Raycast (transform.position, dirToTarget, dstToTarget, obstacleMask)) 
				{
					if(enemyController.CurrentHealth > 0 && characterController.currentHealth > 0)
					{
						visibleTargets.Add (target);
						//gameObject.GetComponent<AIPath>().canMove = true;
					}
				}
			}
		}
		}
	}


	public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal) {
		if (!angleIsGlobal) {
			angleInDegrees += transform.eulerAngles.y;
		}
		return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad),0,Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
	}

}
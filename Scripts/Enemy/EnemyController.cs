﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour {

	[SerializeField]
	private float maxHealth = 100;

	private AIPath aiPath;

	public GameObject healthUI;
	public Slider healthBar;

	private Material meshMaterial;
	private Color originalMeshColor;

	public Text healthText;

	private float currentHealth;

	public float CurrentHealth
	{
		get
		{
			return currentHealth;
		}
	}

	private Animator enemyAnimator;

	public float timeBetweenAttacks = 1f;
	public int attackDamage = 10;
	bool playerInRange;
	float timer;
	private PlayerController characterController;


	void Awake()
	{
		currentHealth = maxHealth;
		enemyAnimator = gameObject.GetComponent<Animator>();
		characterController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
		aiPath = GetComponent<AIPath>();
		meshMaterial = GetComponentInChildren<SkinnedMeshRenderer>().material;
		originalMeshColor = meshMaterial.GetColor("_Color");
		healthUI = GameObject.Find("EnemyUI");
		healthBar = GameObject.Find("HealthBar").GetComponent<Slider>();
		healthText = GameObject.Find("HealthText").GetComponent<Text>();
	}

	public void TakeDamage(int damage)
	{
		StartCoroutine("ChangeMeshColor");
		currentHealth -= damage;
		healthText.text = currentHealth.ToString() + " / 100";
		healthBar.value = currentHealth;
		if(currentHealth <= 0)
		{
			healthText.text = "0 / 100";
			healthBar.value = currentHealth;
			Death();
		}
	}

	void Death()
	{
		aiPath.enabled = false;
		enemyAnimator.SetTrigger("Dead");
		SpawnItems();
		gameObject.GetComponent<CapsuleCollider>().enabled = false;
		gameObject.GetComponent<SphereCollider>().enabled = false;
		gameObject.GetComponent<CharacterController>().enabled = false;
		Invoke("Destroy",5f);
	}

	void SpawnItems()
	{
		if(Random.Range(0,5) == 4)
		{
		int randomType = UnityEngine.Random.Range(0,3);

		GameObject tmp = Instantiate(InventoryManager.Instance.itemObject,new Vector3(transform.position.x,0.2f,transform.position.z),Quaternion.identity) as GameObject;


		int randomItem;

		ItemScript newItem = tmp.AddComponent<ItemScript>();
		switch(randomType)
		{
		case 0:
			randomItem = UnityEngine.Random.Range(0,InventoryManager.Instance.ItemContainer.Consumables.Count);
			newItem.Item = InventoryManager.Instance.ItemContainer.Consumables[randomItem];
			break;

		case 1:
			randomItem = UnityEngine.Random.Range(0,InventoryManager.Instance.ItemContainer.Weapons.Count);
			newItem.Item = InventoryManager.Instance.ItemContainer.Weapons[randomItem];
			break;

		case 2:
			randomItem = UnityEngine.Random.Range(0,InventoryManager.Instance.ItemContainer.Equipment.Count);
			newItem.Item = InventoryManager.Instance.ItemContainer.Equipment[randomItem];
			break;
		}
		tmp.transform.rotation = newItem.mesh.transform.rotation;
		tmp.transform.localScale = newItem.mesh.transform.lossyScale;
		tmp.GetComponent<MeshRenderer>().sharedMaterials = newItem.mesh.GetComponent<MeshRenderer>().sharedMaterials;
		tmp.GetComponent<MeshFilter>().sharedMesh = newItem.mesh.GetComponent<MeshFilter>().sharedMesh;
		tmp.AddComponent<BoxCollider>();
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Player") == true)
		{
			playerInRange = true;
			enemyAnimator.SetBool("PlayerInRange",true);
			enemyAnimator.SetInteger("AttackState", Random.Range(0,3));
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.CompareTag("Player") == true)
		{
			playerInRange = false;
			enemyAnimator.SetBool("PlayerInRange",false);
		}
	}

	void Update ()
	{
		// Add the time since Update was last called to the timer.
		timer += Time.deltaTime;

		// If the timer exceeds the time between attacks, the player is in range and this enemy is alive...
		if(timer >= timeBetweenAttacks && playerInRange && currentHealth > 0)
		{
			// ... attack.
			AttackPlayer ();
		}
	}

	void AttackPlayer()
	{
		timer = 0f;

		if(characterController.currentHealth > 0)
		{
			characterController.TakeDamage(attackDamage);
		}
		else if(characterController.currentHealth <= 0)
		{
			playerInRange = false;
			enemyAnimator.SetBool("PlayerInRange",false);
		}
	}

	void OnMouseEnter()
	{
		if(currentHealth >= 0)
		{
		// Showing Health Bar
		//healthUI.SetActive(true);
		healthBar.gameObject.SetActive(true);
		healthBar.enabled = true;

		// Showing the Health Text.true
		healthText.gameObject.SetActive(true);
		healthText.enabled = true;
		healthText.GetComponent<Text>().text = currentHealth.ToString() + " / 100";
		healthBar.value = currentHealth;
		}
	}

	void OnMouseExit()
	{
		healthText.enabled = false;
		healthText.gameObject.SetActive(false);
		healthBar.enabled = false;
		healthBar.gameObject.SetActive(false);
	}

	IEnumerator ChangeMeshColor()
	{
		meshMaterial.SetColor("_Color",Color.red);
		yield return new WaitForSeconds(.5f);
		meshMaterial.SetColor("_Color",originalMeshColor);
	}

	void Destroy()
	{
		Destroy(gameObject);
	}
}

﻿using UnityEngine;
using System.Collections;

public class ChestScript : MonoBehaviour 
{
	private Inventory chestInventory;
	private Animation animation;
	private PlayerController player;
	private bool wasOpenedBefore = false;
	public bool canOpenChest = false;

	bool isOpen = false;

	void Awake()
	{
		animation = GetComponent<Animation>();
		player = GameObject.FindObjectOfType<PlayerController>();
		chestInventory = GameObject.Find("ChestInventory").GetComponent<Inventory>();
	}

	void OnMouseUp()
	{
		if(canOpenChest == true)
		{
		if(isOpen == false)
		{
			animation.Play("open");
			isOpen = true;
			chestInventory.Open();
			if(wasOpenedBefore == false)
			{
					
				int randomType = UnityEngine.Random.Range(0,3);

				GameObject tmp = Instantiate(InventoryManager.Instance.itemObject);

				int randomItem;

				tmp.AddComponent<ItemScript>();

				ItemScript newItem = tmp.GetComponent<ItemScript>();

				switch(randomType)
				{
				case 0:
					randomItem = UnityEngine.Random.Range(0,InventoryManager.Instance.ItemContainer.Consumables.Count);
					newItem.Item = InventoryManager.Instance.ItemContainer.Consumables[randomItem];
					break;

				case 1:
					randomItem = UnityEngine.Random.Range(0,InventoryManager.Instance.ItemContainer.Weapons.Count);
					newItem.Item = InventoryManager.Instance.ItemContainer.Weapons[randomItem];
					break;

				case 2:
					randomItem = UnityEngine.Random.Range(0,InventoryManager.Instance.ItemContainer.Equipment.Count);
					newItem.Item = InventoryManager.Instance.ItemContainer.Equipment[randomItem];
					break;

				}
				chestInventory.AddItem(newItem);
				Destroy(tmp);
				wasOpenedBefore = true;
			}
		}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Player")
		{
			canOpenChest = true;
		}
	}

	void OnTriggerExit(Collider other)
	{
		if(other.tag == "Player")
		{
			if(isOpen == true)
			{
				animation.Play("close");
				isOpen = false;
			}
			if(chestInventory.IsOpen)
			{
				chestInventory.Open();
			}
			canOpenChest = false;
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Consumable : Item {

	public int Health{get;set;}

	public int Mana{get;set;}

	private Slider healthSlider;

	private PlayerController player;

	void Initialize()
	{
		healthSlider = GameObject.Find("HealthOrb").GetComponent<Slider>();
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
	}

	public Consumable()
	{}

	public Consumable(string itemName, string description, ItemType itemType, Quality quality, string spriteNeutral, string spriteHighlight, int maxSize,int animationNumber,string mesh, int health, int mana) 
		: base(itemName,description,itemType,quality,spriteNeutral, spriteHighlight, maxSize,animationNumber, mesh )
	{
		this.Health = health;
		this.Mana = mana;
	}

	public override void Use(Slot slot, ItemScript item)
	{
		Debug.Log("Used " + ItemName);
		Initialize();

		//Debug.Log(player.name);
		Debug.Log(Health.ToString());
		player.currentHealth += Health;
		healthSlider.value += Health;
		slot.RemoveItem();
	}

	public override string GetTooltip()
	{
		//Creare an empty string
		string stats = string.Empty;

		if (Health > 0)
		{
			stats += "\n Restores" + Health.ToString() + " Health";
		}
		if (Mana > 0)
		{
			stats += "\n Restores" + Mana.ToString() + " Mana";
		}

		//get tooltip from base class
		string itemTip = base.GetTooltip();

		return string.Format("{0}" + "<size=14>{1}</size>",itemTip,stats);
	}
}

﻿using UnityEngine;
using System.Collections;

public class Equipment : Item {


	public int Strength{get;set;}

	public int Intellect{get;set;}

	public int Agility{get;set;}

	public int Stamina{get;set;}

	public Equipment()
	{

	}

	public Equipment(string itemName, string description, ItemType itemType, Quality quality, string spriteNeutral, string spriteHighlight, int maxSize,int animationNumber, string mesh, int strength, int intellect, int agility, int stamina)
		: base(itemName,description,itemType,quality,spriteNeutral,spriteHighlight,maxSize,animationNumber, mesh)
	{
		this.Strength = strength;
		this.Intellect = intellect;
		this.Agility = agility;
		this.Stamina = stamina;
	}

	public override void Use(Slot slot, ItemScript item)
	{
		CharacterPanel.Instance.EquipItem(slot, item);
	}

	public override string GetTooltip()
	{
		string stats = string.Empty;

		if (Strength > 0)
		{
			stats += "\n+" + Strength.ToString() + " Strength";
		}
		if (Intellect > 0)
		{
			stats += "\n+" + Intellect.ToString() + " Intellect";
		}
		if (Agility > 0)
		{
			stats += "\n+" + Agility.ToString() + " Agility";
		}
		if (Stamina > 0)
		{
			stats += "\n+" + Stamina.ToString() + " Stamina";
		}

		string itemTip = base.GetTooltip();
		
		return string.Format("{0}" + "<size=14>{1}</size>",itemTip,stats); 
	}

}

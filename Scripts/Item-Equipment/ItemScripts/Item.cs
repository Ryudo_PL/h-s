﻿using UnityEngine;
using System.Collections;

public abstract class Item {

	public ItemType ItemType{get; set;}

	public Quality Quality{get; set;}

	public string SpriteNeutral{get; set;}

	public string SpriteHighlight{get; set;}

	public int MaxSize{get; set;}

	public string ItemName{get; set;}

	public string Description{get; set;}

	public string ItemMesh{get;set;}

	public int Animation{get;set;}

	public Item() //needed for XML document
	{

	}

	public Item(string itemName, string description, ItemType itemType, Quality quality, string spriteNeutral, string spriteHighlight, int maxSize,int animationNumber, string mesh)
	{
		this.ItemName = itemName;
		this.Description = description;
		this.ItemType = itemType;
		this.Quality = quality;
		this.SpriteNeutral = spriteNeutral;
		this.SpriteHighlight = spriteHighlight;
		this.MaxSize = maxSize;
		this.Animation = animationNumber;
		this.ItemMesh = mesh;
	}

	public abstract void Use(Slot slot, ItemScript item);

	public virtual string GetTooltip()
	{
		string stats = string.Empty;  //Resets the stats info
		string color = string.Empty;  //Resets the color info
		string newLine = string.Empty; //Resets the new line
		
		if (Description != string.Empty) //Creates a newline if the item has a description, this is done to makes sure that the headline and the describion isn't on the same line
		{
			newLine = "\n";
		}
		
		switch (Quality) //Sets the color accodring to the quality of the item
		{
		case Quality.COMMON:
			color = "white";
			break;
		case Quality.UNCOMMON:
			color = "lime";
			break;
		case Quality.RARE:
			color = "navy";
			break;
		case Quality.EPIC:
			color = "magenta";
			break;
		case Quality.LEGENDARY:
			color = "orange";
			break;
		case Quality.ARTIFACT:
			color = "red";
			break;
		}

		return string.Format("<color=" + color + "><size=16>{0}</size></color><size=14><i><color=lime>" + newLine + "{1}</color></i>\n{2}</size>", ItemName,Description,ItemType.ToString().ToLower());
		//return null;
	}
		


}

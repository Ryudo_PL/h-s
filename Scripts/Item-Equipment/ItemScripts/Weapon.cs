﻿using UnityEngine;
using System.Collections;

public class Weapon : Equipment {

	public float AttackSpeed{get;set;}
	
	public Weapon()
	{
		
	}

	public Weapon(string itemName, string description, ItemType itemType, Quality quality, string spriteNeutral, string spriteHighlight, int maxSize,int animationNumber, string mesh,int strength, int intellect, int agility, int stamina, float attackSpeed)
		: base(itemName,description,itemType,quality,spriteNeutral,spriteHighlight,maxSize,animationNumber, mesh,strength,intellect,agility,stamina)
	{
		this.AttackSpeed = attackSpeed;
	}

	public override void Use(Slot slot, ItemScript item)
	{
		CharacterPanel.Instance.EquipItem(slot, item);
	}

	public override string GetTooltip()
	{
		string equipmentTip = base.GetTooltip();


		return string.Format("{0} \n <size=14> Attack Speed: {1}</size>",equipmentTip,AttackSpeed);
	}
}

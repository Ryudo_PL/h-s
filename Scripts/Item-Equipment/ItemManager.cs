﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml.Serialization;
using System.IO;

public enum Category {EQUIPMENT, WEAPON, CONSUMABLE}

public class ItemManager : MonoBehaviour {

	public ItemType itemType;

	public Quality quality;

	public Category category;

	public string spriteNeutral;

	public string spriteHighlight;

	public int maxSize;

	public string itemName;

	public string description;

	public int strength;

	public int intellect;

	public int agility;

	public int stamina;

	public float attackSpeed;

	public int health;

	public int mana;

	public int animationNumber;

	public string mesh;

	public void CreateItem()
	{
		ItemContainer itemContainter = new ItemContainer();

		Type[] itemTypes = {typeof(Equipment), typeof(Weapon),typeof(Consumable)};

		FileStream fs = new FileStream(Path.Combine(Application.streamingAssetsPath,"Items.xml"), FileMode.Open);

		XmlSerializer serializer = new XmlSerializer(typeof(ItemContainer),itemTypes);

		itemContainter = (ItemContainer)serializer.Deserialize(fs);

		serializer.Serialize(fs,itemContainter);

		fs.Close();

		switch(category)
		{
		case Category.EQUIPMENT:
			itemContainter.Equipment.Add(new Equipment(itemName,description,itemType,quality,spriteNeutral,spriteHighlight,maxSize,animationNumber, mesh,strength,intellect,agility,stamina));
			break;

		case Category.WEAPON:
			itemContainter.Weapons.Add(new Weapon(itemName,description,itemType,quality,spriteNeutral,spriteHighlight,maxSize,animationNumber,mesh,strength,intellect,agility,stamina,attackSpeed));
			break;

		case Category.CONSUMABLE:
			itemContainter.Consumables.Add(new Consumable(itemName,description,itemType,quality,spriteNeutral,spriteHighlight, maxSize,animationNumber, mesh,health,mana));
			break;
		}

		fs = new FileStream(Path.Combine(Application.streamingAssetsPath,"Items.xml"), FileMode.Create);
		serializer.Serialize(fs,itemContainter);
		fs.Close();
	}

}

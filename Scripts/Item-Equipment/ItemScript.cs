﻿using UnityEngine;
using System.Collections;

public enum ItemType {CONSUMABLE, MAINHAND, TWOHAND, OFFHAND, HEAD, NECK, CHEST, RING, LEGS, BRACES, BOOTS, TRINKET, SHOULDERS, BELT, GENERIC, GENERICWEAPON};
public enum Quality {COMMON,UNCOMMON,RARE,EPIC,LEGENDARY,ARTIFACT}

public class ItemScript : MonoBehaviour 
{
	public Sprite spriteNeutral;
	public Sprite spriteHighlighted;
	public GameObject mesh;
	private Item item;

	public Item Item
	{
		get{return item;}
		set
		{
			item = value;
			spriteHighlighted = Resources.Load<Sprite>(value.SpriteHighlight);
			spriteNeutral = Resources.Load<Sprite>(value.SpriteNeutral);
			mesh = Resources.Load<GameObject>(value.ItemMesh);
		}
	}

	private Inventory inventory;

	void Awake()
	{
		inventory = GameObject.Find("Inventory").GetComponent<Inventory>();
	}

	/// <summary>
	/// Uses the item
	/// </summary>
	public void Use(Slot slot)
	{
		item.Use(slot,this);

	}
	
	public string GetTooltip()
	{
		return item.GetTooltip();
	}

	void OnMouseDown()
	{
		if(inventory.AddItem(gameObject.GetComponent<ItemScript>()))
		{
			Destroy(gameObject);
		}
	}
	
}


﻿using UnityEngine;
using System.Collections;
using System;

public class CharacterPanel : Inventory {

	public Slot[] equipmentSlots;

	private PlayerController player;

	private static CharacterPanel instance;

	public static CharacterPanel Instance
	{
		get
		{
			if(instance == null)
			{
				instance = GameObject.FindObjectOfType<CharacterPanel>();
			}
			return CharacterPanel.instance;
		}
	}

	public Slot WeaponSlot
	{
		get{return equipmentSlots[9];}
	}

	public Slot OffHandSlot
	{
		get{return equipmentSlots[10];}
	}

	public override void CreateLayout ()
	{

	}

	public void EquipItem(Slot slot, ItemScript item)
	{
		if(item.Item.ItemType == ItemType.MAINHAND || item.Item.ItemType == ItemType.TWOHAND && OffHandSlot.IsEmpty)
		{
			player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
			Slot.SwapItems(slot, WeaponSlot);
			switch(item.Item.Animation)
			{
			case 1:
				player.WeaponState = 1;
				break;
			case 2:
				player.WeaponState = 2;
				break;
			case 4:
				player.WeaponState = 4;
				break;
			case 7:
				player.WeaponState = 7;
				break;
			case 8:
				player.WeaponState = 8;
				break;
			case 9:
				break;
			}
			player.animator.SetInteger("WeaponState",item.Item.Animation);
		}
		else
		{
			Slot.SwapItems(slot, Array.Find(equipmentSlots, x => x.canContain == item.Item.ItemType));
		}

	}

	public override void ShowToolTip(GameObject slot)
	{
		//Saves a reference to the slot we just moused over
		Slot tmpSlot = slot.GetComponent<Slot>();
		
		//If the slot contains an item and we arent splitting or moving any items then we can show the tooltip
		if (slot.GetComponentInParent<Inventory>().IsOpen && !tmpSlot.IsEmpty && InventoryManager.Instance.HoverObject == null && !InventoryManager.Instance.selectStackSize.activeSelf)
		{
			//Gets the information from the item on the slot we just moved our mouse over
			InventoryManager.Instance.visualTextObject.text = tmpSlot.CurrentItem.GetTooltip();
			
			//Makes sure that the tooltip has the correct size.
			InventoryManager.Instance.sizeTextObject.text = InventoryManager.Instance.visualTextObject.text;
			
			//Shows the tool tip
			InventoryManager.Instance.tooltipObject.SetActive(true);
			
			//Calculates the position while taking the padding into account
			float xPos = slot.transform.position.x;
			float yPos = slot.transform.position.y;
			
			//Sets the position
			InventoryManager.Instance.tooltipObject.transform.position = new Vector2(xPos, yPos);
		}
	}

	public void CalculateStats()
	{
		int strength = 0;
		int stamina = 0;
		int intellect = 0;
		int agility = 0;

		foreach(Slot slot in equipmentSlots)
		{
			if(!slot.IsEmpty)
			{
				Equipment e = (Equipment)slot.CurrentItem.Item;
				strength += e.Strength;
				stamina += e.Stamina;
				intellect += e.Intellect;
				agility += e.Agility;
			}
		}

		GameObject.FindObjectOfType<PlayerController>().SetStats(strength,stamina,intellect,agility);
	}

	public override void SaveInventory()
	{
		string content = string.Empty;

		for(int i = 0; i < equipmentSlots.Length; i++)
		{
			if(!equipmentSlots[i].IsEmpty)
			{
				content += i + "-" + equipmentSlots[i].Items.Peek().Item.ItemName + ";";
			}
		}

		PlayerPrefs.SetString("CharPanel", content);
		PlayerPrefs.Save();
	}

	public override void LoadInventory()
	{
		foreach(Slot slot in equipmentSlots)
		{
			slot.ClearSlot();
		}
		
		string content = PlayerPrefs.GetString("CharPanel");
		string[] splitContent = content.Split(';');

		for(int i = 0; i < splitContent.Length - 1;i++)
		{
			string[] splitValues = splitContent[i].Split('-');
			int index = Int32.Parse(splitValues[0]);
			string itemName = splitValues[1];

			GameObject loadedItem = Instantiate(InventoryManager.Instance.itemObject);

			loadedItem.AddComponent<ItemScript>();

			if(index == 9 || index == 10)
			{
				loadedItem.GetComponent<ItemScript>().Item = InventoryManager.Instance.ItemContainer.Weapons.Find(x => x.ItemName == itemName);
			}
			else
			{
				loadedItem.GetComponent<ItemScript>().Item = InventoryManager.Instance.ItemContainer.Equipment.Find(x => x.ItemName == itemName);
			}

			equipmentSlots[index].AddItem(loadedItem.GetComponent<ItemScript>());

			Destroy(loadedItem);
			CalculateStats();
		}
	}

}

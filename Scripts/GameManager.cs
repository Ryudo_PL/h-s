﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public static GameManager instance;
	public GameObject[] enemiesList;
	public GameObject[] instantiators;
	public GameObject ladder;

	[HideInInspector]
	public GameObject healthUI;
	[HideInInspector]
	public Slider healthBar;
	[HideInInspector]
	public Text healthText;

	void Awake()
	{
		//Check if instance already exists
		if (instance == null)

			//if not, set instance to this
			instance = this;

		//If instance already exists and it's not this:
		else if (instance != this)

			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);	

		//Sets this to not be destroyed when reloading scene
		DontDestroyOnLoad(gameObject);

		healthUI = GameObject.Find("EnemyUI");
		healthBar = GameObject.Find("HealthBar").GetComponent<Slider>();
		healthText = GameObject.Find("HealthText").GetComponent<Text>();
		instantiators = GameObject.FindGameObjectsWithTag("Instantiator");
	}

	void Start()
	{
		SpawnEnemies();
	}

	private void SpawnEnemies()
	{
		foreach(GameObject instantiator in instantiators)
		{
			for(int i = 0; i < Random.Range(4,10); i++)
			{
				GameObject enemy = Instantiate(enemiesList[Random.Range(0,enemiesList.Length)], instantiator.transform.position,Quaternion.identity) as GameObject;
			}
		}
		healthText.gameObject.SetActive(false);
		healthBar.gameObject.SetActive(false);
	}
}
